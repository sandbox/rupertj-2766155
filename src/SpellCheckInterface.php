<?php

namespace Drupal\search_api_spellcheck;

interface SpellCheckInterface {
  /**
   * Takes a string and returns either the string with spelling suggestions or
   * FALSE if none could be found.
   *
   * @param string $original_string
   *   A single word or full sentance.
   *
   * @return
   *   A string if any suggestions could be applied or FALSE if none could be
   *   applied.
   */
  public function suggestString($original_string);
}